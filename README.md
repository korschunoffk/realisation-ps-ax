# realisation PS ax

Скрипт psax необходимо запускать с root правами, т.к. парсится `/proc/<PID>/fd`, с обычными правами доступа нет.

В первой части формируем список процессов для парсинга посредствам отбора всех числовых названий папок в /proc

```
proc=/proc/*[0-9]
    for item in $proc
        do
        echo $item | sed 's+/proc/++g' >> temp
    done

```
Была замечена ситуация, что некоторые процессы перестают существовать во время выполнения скрипта, поэтому далее реализована проверка на актуальность

`if [ -d /proc/$numproc ]; then `

Далее осуществляется парсинг /proc/<PID>/ stat fd cmdline status на интересующие нас столбцы


Для вывода TTY парсим свойства папки FD, где явно указано с каким терминалом связаны файлы.
если ни с каким, то поле TTY задаётся "?"

```
tty=$( ls -l /proc/$numproc/fd | grep '/dev/tty\|/dev/pts' | awk '{ print $11 }' | sed 's+/dev/++g' | head -c 5 )
if [ -z "$tty" ]; then
tty='?'
fi
```

Время берется из суммы 14 и 15 столбцов файла stat и приводится к читабельному виду:

```
time=$( cat /proc/$numproc/stat |awk '{ print $14+$15}' )
time=$(( $time / 100 ))
time=$(date -u -d @$time +%H:%M:%S)
```

Для вывода поля COMMAND спользуется следующая конструкция - читаем из файла cmdline, если там пусто, то берем строку 2 из файла status

```
commandtype=$( cat /proc/$numproc/cmdline | head -c 54 )
if [ -z "$commandtype" ]; then
commandtype=0
commandtype=$( grep 'Name' /proc/$numproc/status | awk '{ print $2 }' )

fi
```
 Выводим результат  для процесса, а после выхода из цикла удаляем временный файл со списком процессов.
 
```
 echo -e $numproc'\t' $tty'\t' $state'\t' $time'\t' $commandtype 
 
 rm -f temp
```


